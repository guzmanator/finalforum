import React, { Component } from 'react';
import Post from './Post/component/Post';
import PostEditor from './PostEditor/component/PostEditor';
import firebase from 'firebase/app';
import 'firebase/database';

import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    
    // hack para asegurar que las funciones tiene el contexto del componente
    this.addPost = this.addPost.bind(this);
    this.updateLoacState = this.updateLoacState.bind(this);

    this.state = {
      posts: []
    }
    
    const config = {
    apiKey: "AIzaSyBDoNDH1lkragIGwqnKb1oUdNftfNSAadk",
    authDomain: "final-forum-b4661.firebaseapp.com",
    databaseURL: "https://final-forum-b4661.firebaseio.com",
    storageBucket: "final-forum-b4661.appspot.com",
    messagingSenderId: "855350605837"
  };
  this.app = firebase.initializeApp(config);
  this.database = this.app.database();
  this.databaseRef =this.database.ref().child('post');


  }
  componentWillMount() {
    const {updateLoacState} = this;
    this.databaseRef.on('child_added', snapshot => {
       const response = snapshot.val();
       updateLoacState(response);
    });
  }
  updateLoacState(response) {
    const posts = this.state.posts;
    const brokenDownPost = response.split(/[\r\n]/g);
    posts.push(brokenDownPost);
    this.setState(posts);
  }
  addPost (newPostBody) {
    console.log(newPostBody);
    // copiar el estado
    const postToSave = newPostBody;
   // guardar en posts
   this.databaseRef.push().set(postToSave);
  }



  render() {
    return (
      <div>
        <div className="panel panel-primary post-editor"> 
          <div className="panel-heading"> 
            <h3 className="panel-title">Message list</h3> 
          </div> 
          {
            this.state.posts.map((postBody, idx) => {
              return (<Post key={idx} postBody={postBody} />)
            })
          }

        </div>
        <div className="panel panel-primary post-editor"> 
          <div className="panel-heading"> 
            <h3 className="panel-title">Add a new message</h3> 
          </div> 
         <PostEditor addPost={this.addPost} />
        </div>
      </div>
    );
  }
}

export default App;
