## Final forum an react app with firebase


#### generando la aplicacion por defecto

empezamos creando un directorio en proyectos y nos movemos dentro
-
```javascript
  mkdir finalForum
  cd finalForum
```

para gilizar utilizaremos una semilal de proyecto que nos facilitara tener una app por defecto puedes ver la info en 
-
```
       https://github.com/facebookincubator/create-react-app
```

para instalarla utiliza
-
```
       npm install -g create-react-app
```

para generar el scafolding de la aplicacion (el . es apra que use el mismo directorio donde estas)
-
```
       create-react-app .
```

comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador veras una pagina con el logo de react
-
```
       http://localhost:3000
```

#### guardar la primera version en git 

creamos un repositorio en bitbucket
-
```
       *ir a bitbucket 

       *repository / create a repository 

       *darle nombre cells-ngob-simple-demo

       *desmarcar privado si aplica 

       *pulsar boton crear 
```

*indicar que quieres un proyecto git 
-
~~~ 
git init
~~~
*en la pagina de despues de crear el repo copiar la ruta para incluir al repo (algo como esto )*
-
~~~ 
       git remote add origin https://guzmanator@bitbucket.org/guzmanator/final-forum.git
~~~

comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "blank react app"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.0 -m "blank react app 0.1.0" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.0
~~~

#### 
eliminar el codigo sobrante e instalar bootstrap

actualizamos el bower json poniendo un nuevo numero de version
-
~~~ 
   "version": "",  
~~~
0.1.1
incluir en dependencias bootstrap y jquery 
-
~~~ 
     npm install bootstrap jquery
~~~

tiene que aparecer en el package .json 
-
~~~ 
   "dependencies": {
     "bootstrap": "^3.3.7",
     "jquery": "^3.1.1",  
~~~

eliminar el css por defecto del app.js
-
~~~ 
  .App {
    text-align: center;
  }

  .App-logo {
    animation: App-logo-spin infinite 20s linear;
    height: 80px;
  }

  .App-header {
    background-color: #222;
    height: 150px;
    padding: 20px;
    color: white;
  }

  .App-intro {
    font-size: large;
  }

  @keyframes App-logo-spin {
    from { transform: rotate(0deg); }
    to { transform: rotate(360deg); }
  }    
~~~

poner una capa como contenido del render en vez del contenido pro defecto (File src/App.js)
-
~~~ 
    import React, { Component } from 'react';
    import './App.css';

    class App extends Component {
      render() {
        return (
          <div>
           hello word
          </div>
        );
      }
    }

    export default App;  
~~~

borrar el fichero /src/logo.svg que ya no usaremos

comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador veras una pagina con el logo de react
-
```
       http://localhost:3000
```
deberias de ver el saludo en vez del contenido por defecto

#### subir la nueva version 0.1.1 al repo 


comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "remove scafolding and add bootstrap and jquery"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.1 -m "remove scafolding and add bootstrap and jquery 0.1.1" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.1
~~~

#### importar librerias externas en la aplicacion para poder usarlas
subimos la version en el package.json 
-
~~~ 
      "version": "0.1.2",
~~~

en el fichero ( src/index.js ) incluimos las dependencias de jquery y bootstrap
-
~~~ javascript
  import React from 'react';
  import ReactDOM from 'react-dom';
  import App from './App';

  // importar el modulo
  import 'jquery';
  // definirlo como variable global disponible en toda la app 
  global.jQuery = require('jquery');
  // importar bootstrap (el modulo)
  require('bootstrap');
  // traer el css de bootstrap en la app 
  import 'bootstrap/dist/css/bootstrap.css';

  import './index.css';

  ReactDOM.render(
    <App />,
    document.getElementById('root')
  );  
~~~

#### subir la nueva version 0.1.2 al repo 


comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "added imports for bootstrap"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.2 -m "added imports for bootstrap 0.1.2" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.2
~~~

#### incluir un panel en la aplicacion 
subimos la version de pakage.json a 0.1.3
-
~~~ 
       "version": "0.1.3",
~~~

copiamos el ejemplo de panel de bootstrap en el render, pero debemos remplazar class por className ya que class es palabra reservada
-
~~~ 
    import React, { Component } from 'react';
    import './App.css';

    class App extends Component {
      render() {
        return (
          <div>
            <div className="panel panel-primary"> 
              <div className="panel-heading"> 
                <h3 className="panel-title">Panel title</h3> 
              </div> 
              <div className="panel-body"> Panel content </div> 
            </div>
          </div>
        );
      }
    }

    export default App;
~~~

comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador veras una pagina con el logo de react
-
```
       http://localhost:3000
```
deberias ver el panel puesto con su cabecera en azul 


#### subir la nueva version 0.1.3 al repo 


comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "added a panel"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.3 -m "added a panel 0.1.3" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.3
~~~

## incluir estilos para hacerlo responsive 

subimos la version a 0.1.4 en el package.json
-
~~~ 
      "version": "0.1.4" 
~~~

creamos una clase post-editor (src/App.css)
-
~~~ 
  .post-editor{
   margin:0 20%;
  }
  @media(max-width: 900px){
    .post-editor{
      margin: 1em;
    }
  }
~~~

incluimos la clase en el panel 
-
~~~ 
    import React, { Component } from 'react';
    import './App.css';

    class App extends Component {
      render() {
        return (
          <div>
            <div className="panel panel-primary post-editor"> 
              <div className="panel-heading"> 
                <h3 className="panel-title">Panel title</h3> 
              </div> 
              <div className="panel-body"> Panel content </div> 
            </div>
          </div>
        );
      }
    }

    export default App; 
~~~

comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador veras una pagina con el logo de react
-
```
       http://localhost:3000
```
deberias de ver el panel centrado y si cambias el tamaño de la ventana este se recoloca centrado


#### subir la nueva version 0.1.4 al repo 


comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "added responsive styles"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.4 -m "added responsive styles 0.1.4" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.4
~~~

#### incluir la caja para escribir comentarios 

subimos la version en el package.json
-
~~~ 
     "version": "0.1.5",
~~~

cambiamos el contenido del panel por un text area y un boton con als clases de bootstrap "form-control" y "btn-success" ademas añadimos una clase personalizada a cada elemenmto del formulario para facilmente cambiar sus estilos
-
~~~ 
    import React, { Component } from 'react';
    import './App.css';

    class App extends Component {
      render() {
        return (
          <div>
            <div className="panel panel-primary post-editor"> 
              <div className="panel-heading"> 
                <h3 className="panel-title">Panel title</h3> 
              </div> 
              <div className="panel-body"> 
                 <textarea className="form-control post-editor-input" ></textarea>

                 <button type="button" className="text-right btn btn-success post-editor-button">Post</button>
                 
              </div> 
            </div>
          </div>
        );
      }
    }

    export default App; 
~~~

pegamos martillazos para poner el boton ( src/App.css)
-
~~~ 
    .post-editor-button{
      float:right;
      margin-top: 10px;
    }   
~~~


comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador veras una pagina con el logo de react
-
```
       http://localhost:3000
```
deberias de ver el text area para escribir con el boton debajo


#### subir la nueva version 0.1.5 al repo 


comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "first coment box version"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.5 -m "first coment box version 0.1.5" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.5
~~~

#### incluir un panel para los mensajes de los usuarios


subimos la version en el package.json
-
~~~ 
     "version": "0.1.6",
~~~

incluimos encima del anterior un panel  en el (src/App.js)
-
~~~ 
        <div className="panel panel-primary post-editor"> 
          <div className="panel-heading"> 
            <h3 className="panel-title">Message list</h3> 
          </div> 
          <div className="panel-body"> 
            Hi i am a post 
          </div> 
        </div>  
~~~


comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador veras una pagina con el logo de react
-
```
       http://localhost:3000
```
deberias de ver el panel de comentarios con uno por defecto


#### subir la nueva version 0.1.6 al repo 


comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "added default messaje"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.6 -m "added default messaje version 0.1.6" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.6
~~~


#### refactor de app a un componente Post separado

subimos la version en el package.json
-
~~~ 
     "version": "0.1.6",
~~~

en la carpeta src creamos una carpeta para los "Post"
-
~~~ 
      mkdir Post
~~~

nos movemos dentro y creamos una component 
-
~~~ 
    cd Post
    mkdir component 
~~~

nos movemos dentro y creamos un archivo jsx llamado Post
-
~~~ 
     cd component
~~~

importamos React y creamos el componente
-
~~~ 
import React from 'react';

const Post = (props) => (


)
~~~

a continuacion cortamos el html del mensaje y lo pegamos dentro del componete 
-
~~~ 
  import React from 'react';

  const Post = (props) => (
    <div className="panel-body"> 
      Hi i am a post 
    </div> 
  );  
~~~

y lo exportamos para poder usarlo 
-
~~~ 
    export default Post; 
~~~

vamos al (app.js) e importamos el componete nuevo
-
~~~ 
     import Post from './Post/component/Post';
~~~

donde teniamos el html ponemos la tag del componente
-
~~~ 
  <div className="panel panel-primary post-editor"> 
    <div className="panel-heading"> 
      <h3 className="panel-title">Message list</h3> 
    </div> 
   <Post />
  </div>  
~~~

comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador veras una pagina con el logo de react
-
```
       http://localhost:3000
```
deberias de ver el panel con el mensaje igual que antes pero ahora utilizando un componente 


#### subir la nueva version 0.1.5 al repo 


comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "refactor post into a separate component"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.7 -m "refactor post into a separate component version 0.1.7" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.7
~~~


#### tener un estado con un listado de mensajes

subimos la version en el package.json
-
~~~ 
     "version": "0.1.8",
~~~

añadimos un constructor al (App.js)
-
~~~ 
  constructor(props) {
    super(props);
    this.state = {
      posts =['hola desde el estado']
    }
  }
~~~

y sustituimos la llamada al componente de Post una vez por cada vez que tengamos en el array (idx es para que exista un indice diferente para cada elemento)
-
~~~ 
          {
            this.state.posts.map((postBody, idx) => {
              return (<Post key={idx} postBody={postBody} />)
            })
          }
~~~

y hacemos que en Post.jsx se use la variable pasada
-
~~~ 
  import React from 'react';

  const Post = (props) => (
    <div className="panel-body"> 
     { props.postBody }
    </div> 
  );
  export default Post;   
~~~


comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador veras una pagina con el logo de react
-
```
       http://localhost:3000
```
deberias de ver el panel con el mensaje hola desde el estado


#### subir la nueva version 0.1.8 al repo 


comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "obtener los post desde un estado"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.8 -m "obtener los post desde un estadoversion 0.1.8" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.8
~~~

#### poner la logica de escuchar 

subimos la version en el package.json
-
~~~ 
     "version": "0.1.9",
~~~

borramos el valor por defecto de los mensajes  y creamos una propiedad nueva para el valor que el user pone en el text area
-
~~~ 
     this.state = {
      posts: [],
      newPostBody: ''
    } 
~~~

creamos dos funciones nuevas, una para eschucahe cuando cambia el input y otra controlar el click del boton  
-
~~~ 
  addPost () {

  }
  
  handlePostEditorInputChange(ev){

  }
~~~

para conseguir que el ambito dentro de las funciones sea el componente y no window en el contructor utilizamos el hack para bindear un nuevo bind
-
~~~ 
  constructor(props) {
    super(props);
    
    // hack para asegurar que las funciones tiene el contexto del componente
    this.addPost = this.addPost.bind(this);
    this.handlePostEditorInputChange = this.handlePostEditorInputChange.bind(this);

    this.state = {
      posts: [],
      newPostBody: ''
    }
  }  
~~~

cambiamos el html del text area para que llame a la funcion cuando el usuario lo cambie y para que refleje el valor si cambiamos el estado 
-
~~~ 
      <textarea 
        value={this.state.newPostBody} 
        onChange={this.handlePostEditorInputChange} 
        className="form-control post-editor-input" > 
      </textarea>
~~~

cambiamos el boton para que llame a la funcion que gestiona el click
-
~~~ 
       <button 
         onClick={this.addPost} 
         type="button" 
         className="btn btn-success post-editor-button">
         Post
      </button>        
~~~

actualizamos el estado cuando el usuario cambia el text area 
-
~~~ 
  handlePostEditorInputChange(ev){
    this.setState({
      newPostBody: ev.target.value
    });
  }  
~~~

actualizamos el array de post cuando el usuario pulsa el boton, notese que creamos una copia del estado para cambiarla antes de asignarla de nuevo
-
~~~ 
  addPost () {
    // copiar el estado
    const newState = Object.assign({}, this.state);
    // incluir el nuevo mensaje
    newState.posts.push(this.state.newPostBody);
    // vaciar el input
    newState.newPostBody = '';
    // actualizar el estado
    this.setState(newState);
  }
~~~
comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador veras una pagina con el logo de react
-
```
       http://localhost:3000
```
deberias de poder escribir en el text area  y al pulsar el boton se aparece el mensaje, si haces varias veces cada vez se ve el nuevo estado


#### subir la nueva version 0.1.9 al repo 


comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "reacionar a las acciones del usuario"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.9 -m "reacionar a las acciones del usuario version 0.1.9" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.9
~~~


comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador veras una pagina con el logo de react
-
```
       http://localhost:3000
```
deberias de ver el panel con el mensaje hola desde el estado


#### subir la nueva version 0.1.8 al repo 


comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "obtener los post desde un estado"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.8 -m "obtener los post desde un estadoversion 0.1.8" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.8
~~~


#### refactorizar para tener un componente de envio de post 

subimos la version en el package.json
-
~~~ 
     "version": "0.1.9",
~~~


creamos un directorio para el nuevo componente 
-
~~~ 
  cd src
  mkdir PostEditor
  cd PostEditor
  mkdir component
  cd component
  touch   PostEditor.jsx
~~~

creamos una clase dentro 
-
~~~ 
import React, { Component } from 'react';


class PostEditor extends Component {
  
}

export default PostEditor;     
~~~

queremos que el elemento tenga su estado asi que copiamos el constructor de app.js pero le quitamos la parte que debe de estar en la lista de post
-
~~~ 
    constructor(props) {
    super(props);
    
    // hack para asegurar que las funciones tiene el contexto del componente
    this.handlePostEditorInputChange = this.handlePostEditorInputChange.bind(this);

    this.state = {
      newPostBody: ''
    }
  }   
~~~


copiamos el html del componente en el metodo render

-
~~~ 
  render () {
    return (
        <div className="panel-body"> 
             <textarea value={this.state.newPostBody} onChange={this.handlePostEditorInputChange} className="form-control post-editor-input" ></textarea>

             <button onClick={this.addPost} type="button" className="btn btn-success post-editor-button">Post</button>
             
          </div> 
    )
  }     
~~~

en el html de la app ponenmos la referencia a nuestro componente y le pasamos la funcion que debe de invocar 
-
~~~ 
    <PostEditor addPost={this.addPost} /> 
~~~

para poder usarlo hay que importarlo 
-
~~~ 
     import Post from './PostEditor/component/PostEditor';
~~~


movemos el metodo de controlar el post de la app al componente 
-
~~~ 
  handlePostEditorInputChange(ev){
    this.setState({
      newPostBody: ev.target.value
    });
  }
~~~

creamos un metodo local para el post 
-
~~~ 
  createPost (){
    this.props.addPost(this.state.newPostBody);
    this.setState({newPostBody: ''});
  }
~~~


y cambiamos metodo onclick para que utilice la funcion 
-
~~~ 
      onClick={this.createPost} 
~~~

no olvidar el binding en el contructor 
-
~~~ 
  this.createPost = this.createPost.bind(this);
~~~

eliminamos las referencias al metodo movido al componente y lo de limpiar el inpput 
-
~~~ 
  constructor(props) {
    super(props);
    
    // hack para asegurar que las funciones tiene el contexto del componente
    this.addPost = this.addPost.bind(this);

    this.state = {
      posts: []
    }
  }
~~~

y actualizamos el metodo post para que tenga como parametro lo que le pasa el componente
-
~~~ 
     
  addPost (newPostBody) {
    // copiar el estado
    const newState = Object.assign({}, this.state);
    // incluir el nuevo mensaje
    newState.posts.push(newPostBody);
    // actualizar el estado
    this.setState(newState);
  }

~~~


#### subir la nueva version 0.1.10 al repo 


comprobar que va a subir las cosas 
-
~~~ 
       git status
~~~
incluir los ficheros al git 
-
~~~ 
       git add .
~~~
hacer un comit para decirle a git que quieres los cambios
-
~~~ 
    git commit -m "refactor nuevo componente de post editor"
~~~

subir los cambios al repositorio 
-
~~~ 
    git push -u origin master
~~~

crear una tag 
-
~~~ 
    git tag -a 0.1.10 -m "git commit -m "refactor nuevo componente de post editor" version 0.1.10" 
~~~

subirla al repo 
-
~~~ 
    git push origin 0.1.10
~~~


## crear un proyecto en firebase

nos vamos a la pagina de firebase, pulsamos en ir a la consola, pulsamos en el boton de crear proyecto y rellenamos el formulario
-
~~~ 
    Crear proyecto
    Nombre del proyecto

    final-forum
    País/Región help_outline
    México
~~~

Con el proyecto creado pulsamos añadir base de datos al proyecto y nos ofrece la configuracion 
-
~~~ 
     <script src="https://www.gstatic.com/firebasejs/3.7.3/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBDoNDH1lkragIGwqnKb1oUdNftfNSAadk",
    authDomain: "final-forum-b4661.firebaseapp.com",
    databaseURL: "https://final-forum-b4661.firebaseio.com",
    storageBucket: "final-forum-b4661.appspot.com",
    messagingSenderId: "855350605837"
  };
  firebase.initializeApp(config);
</script>
~~~

ahora es necesario poner firebase en nuestra app
-
~~~ 
     npm install --save firebase
~~~


## poner firebase en nuestra app 

vamos al app.js y importamos firebase
-
~~~ 
    import firebase from 'firebase/app';
    import 'firebase/database';
~~~

En el constructor incluimos la llamada de inicializar firebase
-
~~~ 
constructor(props) {
    super(props);
....
// config copiado de firebase
    const config = {
      apiKey: "AIzaSyBDoNDH1lkragIGwqnKb1oUdNftfNSAadk",
      authDomain: "final-forum-b4661.firebaseapp.com",
      databaseURL: "https://final-forum-b4661.firebaseio.com",
      storageBucket: "final-forum-b4661.appspot.com",
      messagingSenderId: "855350605837"
    };
// referencia al objeto de firebase
    this.app = firebase.initializeApp(config);
//referencia a la base de datos
    this.database = this.app.database();
// referencia a la "tabla" post
    this.databaseRef =this.database.ref().child('post');
  }
~~~


cuando el componente se carga  actualizamos el estado 
-
~~~ 
  componentWillMount() {
    const {updateLoacState} = this;
    this.databaseRef.on('child_added', snapshot => {
       const response = snapshot.val();
       updateLoacState(response);
    });
  }

~~~

creamos una funcion para actualizar el estado
-
~~~ 
   updateLoacState(response) {
   // copia del estado actual
    const posts = this.state.posts;
    // separar por saltos de linea (no se ven en html)
     const brokenDownPost = response.split(/[\r\n]/g);
    // actualizar la copia del estado
    posts.push(brokenDownPost);
    // actualizar el estado
    this.setState(posts);
  }   
~~~

como ahora venimos de firebase y tenemos como elementos arrys de strings nos vamos al componente de post y ponemos un map para pintarlos
-
~~~ 
     <div className="panel-body"> 
      { 
        props.postBody.map((postPart, idx)=>{
          <div>{postPart}</div>
        })
      }
    </div> 
~~~

volvemos a la app y cambiamos addPost para que guarde en firebase
-
~~~ 
     addPost (newPostBody) {
    // copiar el estado
    const postToSave = newPostBody;
   // guardar en posts
   this.databaseRef.push().set(postToSave);
  }
~~~

vamos a firebase, en el menu lateral escogemos database y en la url ponemos 
-
~~~ 
   https://final-forum-b4661.firebaseio.com/post
~~~
se deberia de ver null
-
~~~ 
   
~~~

lo ejecutamos 
-
~~~ 
   npm start 
~~~

y tiene que fallar popr permisos
-
~~~ 
   Uncaught (in promise) Error: PERMISSION_DENIED: Permission denied
~~~

vamos a firebase, a la pestaña reglas
-
~~~ 
{
  "rules": {
    ".read": "auth != true",
    ".write": "auth != true"
  }
}
~~~
